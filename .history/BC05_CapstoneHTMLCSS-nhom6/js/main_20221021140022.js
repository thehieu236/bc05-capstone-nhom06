// Dark theme
/**
Solution: 
Khi click vào nút moon 🌙 , thẻ body sẽ được thêm class dark.
Dựa vào class dark, ta viết css dành riêng cho dark theme.

Ngược lại, khi click vào nút sun ☀️, ta sẽ xóa class dark để css light theme hiện lại

*/

//gọi thẻ button có id là switchButton
// gắn chức năng click cho switchButton
document.getElementById("switchButton").onclick = function () {
	//khi button được click thì tìm đến thẻ body có id là myBody
	// dùng classlist để gọi thuộc tính class của thẻ body
	//Sử dụng toggle() để thêm hoặc xóa 1 class của thẻ html. Nếu thẻ body không có class dark thì toggle sẽ thêm class dark vào body. Ngược lại, nếu thẻ body đang có class dark thì toogle sẽ xóa class dark đi.
	document.getElementById("myBody").classList.toggle("dark");
  };

// Carousel

$(document).ready(function () {
	$(".owl-carousel").owlCarousel({
	  loop: true,
	  margin: 0,
	  nav: false,
	  responsiveClass: true,
	  autoplay: true,
	  autoplayTimeout: 5000,
	  autoplaySpeed: 1000,
	  autoplayHoverPause: false,
	  responsive: {
		0: {
		  items: 1,
		  nav: false,
		},
		480: {
		  items: 1,
		  nav: false,
		},
		667: {
		  items: 1,
		  nav: false,
		},
		1000: {
		  items: 1,
		  nav: false,
		},
	  },
	});
  });

// Counter number
jQuery(document).ready(function ($) {
	$('.counter').counterUp({
		delay: 10,
		time: 1000,
	});
});
